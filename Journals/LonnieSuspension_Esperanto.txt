BONKUNULO LERNEJO

FORMO DE DISCIPLINA PLUSENDO

Nomo de lernanto: JOLANDA DESOTO Grado: 12 Dato: 1994-09-18

S-ro BENĈLO observis F-ino DESOTOn surportante T-ĉemizon kun malakceptebla bildon sur la antaŭo (granda ladskatolo de biero etikedita 'PABSTO BLUA RIBONO.') Oni sendis F-ino DESOTOn al la oficejo de la konsilisto.

Oni donis al F-ino DESOTO la elekto turni ŝian ĉemizon ene ekstere, aŭ vesti ŝin en ĉemizo el ŝia gimnastejoŝranko, aŭ sendiĝi hejmen por la cetero de la tago. F-ino DESOTO elektis sendiĝi hejmen. Oni telefonis al ŝia patro, sed neniu respondis kaj neniu telefona respondilo. F-ino DESOTO devas redoni tiun formon morgaŭ, subskribita per ŝia patro.

SUBSKRIBITA
Geinstrusitaro: MARĈA KANDESLOSKI
Lernanto: LONJO D
Gepatro: