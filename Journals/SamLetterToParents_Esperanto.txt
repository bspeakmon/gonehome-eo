
AL TIU, KIU EBLAS KONCERNITA:

Mi, Samanta GREENBRIAR, havas 17 jarojn, kaj tial estas sendependa, tute funkcianta homo.

Ke vi ankoraŭ malpermesas min de iri en la urbon sole estas, sincere, absurda. Komparu kun Katja, kiu havas nur pli tri da jaroj ol mi, kaj tamen vi permesis ŝin iri TUTE TRANS LA OCEANON AL ALIA KONTINENTO sole.

Mi nur volas pasi vesperon en normala, tute sekura urbo SOLE kiel PERSONO kaj ĉar vi eble ankaŭ memoras, ke mi havas mian propran aŭton, vi NE VERE POVAS HALTIGI MIN.

Plej varmaj respektoj,

Via filino Samanta