Kara Ĝanja,

Mi ĝojas, ke vi komuniki kun min denove! La novaĵo pri la nova domo-- ĝi estas vera aventuro! Vi memoras la eta dormejĉambro, kiu ni kunhavigis unuajare? Kiam ni malfeliĉigis, fantaziante pri nia idealaj domoj? Mi ĉiam diris, ke mi volis domego, vi diris, ke vi nur volis dometo en la arbaro... jen vi, kiu ricevis ambaŭ! Dio devas ami vin! Mi povas uzi tiom da tian magion. Sendu al mi kelkaj nombroj por loterio, mi ludas ilin! Serioze!

Sed mi ne devus plendi pri tiu fidinda malnova fendetaĝa domo, kiu ni havas depost Robĉjo estis transigita al Vinipego. Ni ĵus aĉetis novajn vinilajn murplankojn. Ĉu vi ĵaluzas? Diru al mi, se vi iam volus interŝanĝi...

Do, kiel fartas la knabinoj? Ĉu Katja jam forlasis pri sia grandan Eŭropan aventuron? Parolante de ĵaluza...

Reskribis al mi baldaŭ! Vi mankas al mi, samĉambranino!

-Karola