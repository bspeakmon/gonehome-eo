Estimata Samanta GREENBRIAR,

Ni amus gratuli vin pri via akcepto al la Kreiva Skribado kurso de la Rido Kolegio Somera Kursaro por Junaj Lernantoj, 1995 kunsido.

Bazita sur via verkado kaj akademia historio, ni ankaŭguas oferi al vi monohelpon pagi 75% de la Somera Kursaro abonpago kaj taksoj.

La alfiksitaj dokumentoj havas konkretojn pri via horaro, laŭvolaj sekundaraj sesioj, kaj via asignita dormejo.

Ni tre antaŭguas vian partoprenon en la Somera Kursaro. Kaj gratulon denove.


Ĝulia MORISO
Estraro de Akceptaro