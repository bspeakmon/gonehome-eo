Dum la atako de germanaj armeoj, preskaŭ 75% de la personoj en ŝiaj hejmurbeto estas mortigitaj. La cetero, inklusive Essa kaj, por iom da tempo, Borislavo, kunpremas en preskaŭ detruita preĝejo. Li estas blinda; mankas al si kruroj, bandaĝita per ŝiritaj litkovriloj. La ovo de Essa ne renkontas ian ajn spermatozoon. Ĝi dissolvas.

Ĉirkau du semajnoj poste, Boriso ne plu tenas vivon. Essa transdonis siajn porciojn por teni Borison viva, sed fine nenio povas savi lin. Ĉar la parieto de la utero estas nebezonata por gravedo, ĝi eliras tra la vagino.

Essa ĵuras pluvivi. Ŝi ekiras por aliĝi al la pola rezisto kiel aŭdaca spiono kaj sabotistino. Alia ovolo komencas kreski en unu el la ovarioj kaj la procezo rekomencas. Estas nekredebla, kiel la ina korpo scias kiel prepari por gravedo!




PAROLU AL MI!