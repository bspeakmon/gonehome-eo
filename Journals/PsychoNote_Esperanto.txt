Ho ĉu vi estas la nova knabino Samja? Mi ests Tomĉjo mi malantaŭ vi mansignu se vi ricevus tion kaj reskribu.

Saluton Tomĉjo. Jes, mi estas Samanta kaj jes mi novas. Kio novas?

Mi nur penssi ĉar vi novas eble vi dezirus amikon. Mi ankaŭ ne havas mul te da amikoj kaj mi volas demandi ion al vi se vi ne gravas, ĉu gravas? J/N

Ne gravas. Kion vi volas demandi?

Ĉu estis nur via onklo kiu freneziĝis aŭ ĉu via tuta familio frenezemas