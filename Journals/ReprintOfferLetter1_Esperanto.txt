Kara S-ro GREENBRIAR,

Unue, lasu min diri ke mi esperas ke vi fartas bone kiam vi ricevas tiun ĉi leteron-- diable, sentas kiel diodamnita miraklo, ke vi ricevas ĝin iel ajn! Ĉu vi scias kiom da tempo ni klopodas trovi vin?! Ne ĝenu, ni estas nek la feduloj, nek la nigre-vestituloj, nek ia ajn de rampante faŝisman koboldon. Efektive, ni partiiĝas kun vi. Lasu min komenci de komenco.

Nekonata Dimensio estas, vi eble diras, eldonado de Specialaĵo-- ni traktas kun la strangado, la antaŭ-sia-tempo, la perdita-sed-ne-forgesita-de-malgranda-sed-dediĉata-amaso-de-konektitajn-libramantoj tipo de eksternormale amasmarketon-evitante vizia esprimo, kiu rifuzas iel ajn akordiĝadon sed la sia.

Ni havas neegalitan sukceson de post nia komenco antaŭ kvar jaroj, elfosante kaj revivante, kiel Kristo (aŭ Sorĉkadavro), klasika verkoj kiel "Mesaĝo de la Serpentuloj" de N.N. BESTMANO, "ĜI ESTAS ENE MI!" de Jenso KELERO, kaj la ofte malpermesita "Venusaj Karnokomercistoj" de Emilo KRIGERO.

Sed ekde tiam ni malkovris ĉifonajn kopiojn de vian "Hazarda" serio ĉe diversaĵa vendo en preĝejo en Longa Branĉo, NJ, ni klopodas trovi la verkiston de ĉi tiu stranga kaj malhela Usona eksterulo-arto.