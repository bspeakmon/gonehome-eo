Volo kaj Testamento de Oskaro MASONO

Mi, Oskaro MASONO, havante plenan kapablon de menso kaj memoro, kaj post kompleta esploro de miaj valoraĵoj, ja deklaras ĉi tiun mian lastan volon kaj testamenton. La jenaj tenos al la momento de mia forpaso:

1. Mi deklaras, ke mi estas tutvivan loĝanton de Buno Kantono; ke mi ne edziĝas kaj havas neniun infanon.

2. Mi deklaras, ke mi ne havas nenian nepagitan ŝuldon sub mia nomo, al ia ajn kreditoroj, vivantoj aŭ mortintoj.