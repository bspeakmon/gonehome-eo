Kara Jan,

Ho karulino, lasu min diri, mi komprenas, kiel vi sentas. Robĉjo kaj mi havis niaj malaltaj fazoj. Iĝas specon de vivando, efektive... Vi kutimiĝas unu la alian, vi vivas en la sama domo viajn proprajn vivojn, la geknaboj plenkreskiĝas, ili foriras... Mi bedaŭras, mi ne helpas, ĉu ne?! Ne zorgu. Tero konsoligos sin pri tio, kio ĝenas lin, kaj tute estos en ordo denove. Kaj koncerne Samjan maltrankvile? Jen tipa adoleskantino. Nenion zorginde.

Intertempe, tio "regita brulado" -- ĝi estas vera aventuro! Sed ni rapidu! Tiu nova naturparkisto, jen kion mi volas priaŭdi! "Rangero Riko?" Vi devus ŝerci min! Estas tro perfekta! Vi DEVAS diri ĉion al mi... kaj sendu fotojn! Mi volas la tutan pakaĵon! Atendu, ĝi elvenis malĝuste!

Kuraĝiĝu vin ĝis Tero eliros lian malfeliĉan humoron. Kaj intertempe, skribu pli da leteroj al via maljuna amikino Karola! Ŝi amegas ilin!


Karola