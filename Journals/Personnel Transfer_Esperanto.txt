Avizo de Provizora Transigo de Personaro

Bruso PENDLTONO
Estro de Personaro
Ŝtata Forstismo Servado

Por helpi la venontan priskribatan brulado, oni transigas rangeron kun ekspertizo pri la procedo al la stacio ĉe Flintloko Nacia Forsto, ekefike 1994-09-02. Bonvolu pagumi afiŝitan personarodosieron.

Oni ordonas la kontrolantan oficiston ĉe Flintloko Forstismo Stacio, Dojena Medikonservatistino Ĝanisa GREENBRIAR, kontroli transigitan personaron. La daŭrado de transigo baziĝos sur kalkulo de plenumado kune kun la rekomendo de la kontrola oficisto.

Salutas,
Bruso PENDLTONO
1994-08-04