1978-02-03

Tero!

Saluton viro, kiel vi fartas? Mi scias, ke vi estas eldonita aŭtoro nun, sed mia redaktisto ĉe Altfidela Fanatikulo havas tro da recenzoj kiuj devas skribita, kaj li serĉas alian liberprofesiulon. Kompreneble mi pensis pri vi. Vi diris en via lasta letero kiel vi ĝenas vin klopode trovi eldono por via lastan verkon, kaj skribante recenzojn de sonaparatoj estas ege simpla. Sidiĝante hejmen kun glaso da skotviskio, aŭskultante al kelkaj diskoj kaj skribante, kiel ĝi sonas. Kaj poste, estos pagita.

Mi enmetis kelkajn eldonojn de la gazeto por uzi kiel ekzemplo. Se ĝi interesas vin, sendu kelkajn skribaĵojn al mia redaktisto. Kaj diru al li, ke via malnovan kolegian amiko Majĉjo sendas vin! Jen la adreso:

Davido CUPPING
Redaktisto de Recenzojn, Altfidela Fanatikulo Gazeto
88 Daŭningo St, Ĉambraro 4
NJ NJ 10014


Faru tion!

Majĉjo