PROPONO DE PROMOCIO

Estimata Dojena Medikonservatistino GREENBRIAR,

Pro via modela administrado de la Flintloko Preskribita Brulado pasintjare, kaj la bezono de la Servado de sperta personaro por kontroli regionajn aferojn, ni volus oferi al vi la oficon de Regiona Konservatisma Administrado Direktorino, responsa pro aferoj en la nordokcidenta Oregono.

Via oficejo ne plu estas en la Flintloko Nacia Forsto; anstataŭe vi laborus en la Regiona Administrado oficejo, lokita je 128 Bullhorn Rd. Oni petus vian helpon elektonte rangeron kiu plenumus vian eksoficejon.

Bonvolu respondi tiel baldaŭ kiel vi povas rilate al via elekton. Ni tre antaŭguas vian respondon.

Salutas,
Bruso PENDLTONO
1995-02-08