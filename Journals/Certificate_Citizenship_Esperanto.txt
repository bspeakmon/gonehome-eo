La Unuiĝintaj Ŝtatoj de Ameriko

Atesto de Ŝtataniginto

Je la 20a julio, 1972 la Kortumo, trovis ke Ĝanisa Elizabeto Konelo GREENBRIAR, tiam loĝante al 1010 Kvina Strato, Seatlo, Vash., intencas rezidi konstante en la Unuiĝintaj Ŝtatoj, kaj ke ŝi konfirmiĝis kun la aplikeblaj kondiĉoj de tiuj ŝtatanigaj leĝoj, kaj ke meritis esti akceptata en civitaneco, tiam ordonis ke tiu homo tiel iĝis, kaj (ŝ)li estis akceptata kiel civitano de Unuiĝintaj Ŝtatoj de Ameriko.

En atestanto de kio la sigelo de Kortumo estas sube afikŝita tiu 20a tago de julio, en la jaro de nia Dio mil naŭcent kaj sepdek du.

Horaco E. KOLVERO
Skribisto de la Distrikto Kortumo de Usono