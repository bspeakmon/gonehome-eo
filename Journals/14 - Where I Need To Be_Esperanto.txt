Katja... mi tre bedaŭras. Ke mi ne povas esti tie por vidi vin persone. Ke mi ne povas mem diri ĉiuj al vi. Sed mi esperas, kiam vi legas tiun taglibron, kaj vi memoras... ke vi komprenas, kial mi devis fari, kion mi faris. Kaj ke vi ne tristos kaj ne malamas min kaj vi nur scios... ke mi estas kie mi bezonas esti.

Mi tiom amegas, Katja. Mi revidos vin. Iam.

-Samja