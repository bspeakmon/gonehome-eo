La bando de Todo perdis sian kantiston-- Todo diris ke li aĉas, Lonjo diris ke li ne plu toleri Todon-- kaj li plendis pri bezono de nova kantisto, kaj Lonjo diris, "Mi povas kanti!" Ni kriis, "Ĉu vere??" kaj ŝi diris, "eble!"

Sed ŝi kunprovis kun ili de semajno nun, kaj mi finfine spektis ilin hodiaŭ en la kelo de Todd, kaj ŝi estas efektive... tre... mirinda. Mi sentas tiom... FIERE, kiam ŝi estas sur scenejo. Estas nekredebla, mirigite iun, kiun vi amas.

Do ĉiuj komprenas, ke ĝi estas nekonstanta situacio, ĝis ŝi varbos en junio. Sed ĝis tiam... mi ĉeestos ĉiujn spektaklojn.