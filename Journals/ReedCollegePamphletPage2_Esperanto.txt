REED KOLEGIO, el la nacio unu da plej laŭdataj privataj liberalartoj kaj sciencoj kolegioj, fiere anoncas sian 14an jaran Somera Kursaro por Junaj Lernantoj. Tiu ĉi kursaro invitas studentoj de gradoj 10 kaj 11 vizitadi diskursojn, kunlaborojn, kaj paroladojn pri larga diverso de intelektaj okupoj. Pro fokusante sur mallargaj groupsesionj kaj detalaj interagadoj inter fakultato kaj studento, la kursaro kuraĝigas junajn mensojn plene esplori lokojn de kreskado kaj riĉiĝiado dum ili proksimiĝas la bordon de pli alta edukado.

Preter la profitoj de la kursaro mem, oni oferos al tri studentoj de ĉiu kurso plenan stipendion por iliaj unua jaro ĉe Reed, se ili decidos vizitadi la kolegion plentempe kiel studentoj kaj estos akceptita de la estraro de akceptado de Reed.

Oni donas al edukistoj ĉe mezlernejoj de studentoj dokumentadon pri la specifaj postuloj pri ĉiu kurso kaj procedoj por sendi verkadojn kaj aplikojn. Studentoj kiu estas akceptitaj de la kursaro estos sciigita dum la komenco de la kalendara jaro 1995.

ESPLORU. LERNU.