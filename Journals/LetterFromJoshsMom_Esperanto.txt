
Ĝanisa,

Dankon pro gastigante Dacĵon al via nova domo. Lia amikino Samanta en la naĵbaraĵo tre mankas al li.

Dacĵo demandis se li povas pruntedoni sian Nintendon Strato Batalante kasedon al Samanta, kaj mi permesis. Li devus malplipasigi kun tiajn ludojn ĉiaokaze.

Diru al Samanta ke ŝi estas bonvolita viziti nin al nia domo iam ajn.


Salutas,

Maria ĈUTZO