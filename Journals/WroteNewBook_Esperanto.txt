El la plumo de Terenco L. GREENBRIAR

Kara Kazo,

Mi ne povas diri al vi, kiel ĝue mi sentas, vidi Johano RUSELO eldonita denove. Multajn dankojn al vi por sendi kopiojn de la novaj eldonoj. La kovroarto ja bonegas!

Mi scias, ke vi diris ke Nekonata Dimensio ne serĉas novajn verkojn por eldoni, sed ĉi tiu reviviĝata intereso en mia verkado lasigas fonton da inspiro, rezultigante manuskripton, kiu finigas la vojaĝo de Johano RUSELO, kiun mi pensas eble interesa al vi. Ĝi estas kontempla kaj introspekta, sed ne forgesante eksciton kaj "strangemo", por kiujn la legantoj de Nekonata Dimensio esperas.

Mi esperas, ke ĝi eble estas ekscitante novan celon por Nekonata Dimensio ĉasi. Minimume, mi dankemas, ke la aventuroj de Johano RUSELO ne finis tiam, ĝuste kiam mi pensis.

Kun miaj dankoj kaj estimoj,
Terenco L. GREENBRIAR