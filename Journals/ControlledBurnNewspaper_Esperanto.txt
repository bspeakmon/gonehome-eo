Regita Brulado Planita en Buno Kantono

Tufoj da fumo plialtiĝos super la nordokcidenta regiono de Buno Kantono dum la plejparto de la venonta semajno, kiel parto de Servado de Fortisto-organizita regita brulado de trokreskitaj sekcioj de Flintloko Nacia Forsto.

Forstistoj preparas la loko dum kelkaj monatoj. La brulado okazos tra la 8a kaj la 17a lunde, marde, merkrede, kaj eble daŭrante ĝis ĵaŭdo dependante la rapidon de progreso, laŭ la Servado de Forstisto.

Krome fortirante nevivantaj kaj trokreskitaj vegetaĝoj kiuj povas ekbruliĝi en la pli sekvaj monatoj, la okazo servos kiel "valora trejnado ekzerco" por la forstistoj kaj kontraŭfajristoj engaĝitaj, diris Dojena Medikonservatistino Ĝanisa GREENBRIAR.