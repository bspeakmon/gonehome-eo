-----ENGLISH-----

Hello, friends! This is my translation of Gone Home into the international
language Esperanto. This work is compatible with and tested on Gone Home
version 1.01 and later.

Notes:

* Commentary subtitles are not (yet) translated.
* A few bits of text and on-screen display were not translatable.
* I have generally left proper names untranslated, especially for celebrities,
  bands or cultural events. I have translated some place names that have
  well-known Esperanto equivalents, or names of bands or songs when important
  to the story.
* Spelling errors in the text are on purpose. Grammatical errors are not,
  especially not in the controls. Please see the "How to contribute" section
  below to have those fixed :)

How to install:

* Download the zip from https://bitbucket.org/bspeakmon/gonehome-eo/downloads/gonehome-eo.zip.
* Unzip the file in the following directories, depending on your OS:
  - Windows Vista/7: /Users/<username>/AppData/LocalLow/The Fullbright Company/Gone Home/Text/Localized/
  - Windows XP: /Documents and Settings/<username>/Local Settings/Application Data/The Fullbright Company/Gone Home/Text/Localized/
  - Mac OS X: ~/Library/Caches/The Fullbright Company/Gone Home/Text/Localized/
  - Linux: ~/.config/unity3d/The Fullbright Company/Gone Home/Text/Localized/
* In the game, select Options from the main menu. Click Languages, and you
  should see Esperanto as a language option.

Licensing:

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike
3.0 Unported License.
http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US

How to contribute:
The translation is hosted at https://bitbucket.org/bspeakmon/gonehome-eo. If
you have suggestions or find bugs, please visit! I will be happy to take your
contributions.

For more about Esperanto:
http://lernu.net

Big thanks to The Fullbright Company for a wonderful game experience and for
making the tools available to build this translation!

-----ESPERANTO-----

Saluton, karaj samideanoj! Tiu ĉi estas mia traduko de Irinta Hejmen (Gone Home)
en la internacian lingvon Esperanto. Tiu ĉi verko estas kongrua kun kaj testita
sur Gone Home versio 1.01 aŭ pli lasta.

Notoj:

* Komentariaj subtitloj ne tradukiĝis.
* Kelkaj pecoj da tekstoj kaj ekranaĵoj netradukeblas.
* Mi ĝenerale ne tradukis nomojn, speciale tiuj de famuloj, bandoj aŭ kulturaj
  aferoj. Mi tradukis kelkajn loknomojn, kiuj havas konatajn egalvalorojn Esperante,
  aŭ nomoj de bandoj aŭ kantoj, kiam ili gravas por la historio.
* Eraroj de literumo en la teksto estas intencaj, sed ne eraroj de gramatiko,
  speciale ne en la kontroloj. Bonvolu rimarki la "Kiel kontribui" sekcio sube
  por korekti ilin :)

Kiel instali:

* Elŝutu la zip-dosieron de https://bitbucket.org/bspeakmon/gonehome-eo/downloads/gonehome-eo.zip.
* Maldensigu la dosieron en unu el la jenajn dosierujojn laŭ via operaciumo:
  - Vindozo Vista/7: /Users/<username>/AppData/LocalLow/The Fullbright Company/Gone Home/Text/Localized/
  - Vindozo XP: /Documents and Settings/<username>/Local Settings/Application Data/The Fullbright Company/Gone Home/Text/Localized/
  - Makintoŝo OS X: ~/Library/Caches/The Fullbright Company/Gone Home/Text/Localized/
  - Linukso: ~/.config/unity3d/The Fullbright Company/Gone Home/Text/Localized/
* En la ludo, elektu Opcioj de la ĉefa menuo. Klaku Lingvoj, kaj vi povus vidi
  Esperanto kiel lingva opcio.

Permesilo:

Ĉi tiu verko estas disponebla laŭ la permesilo Krea Komunaĵo Atribuite-Nekomerce-Samkondiĉe
3.0 Neadaptita.
http://creativecommons.org/licenses/by-nc-sa/3.0/deed.eo

Kiel kontribui:
La traduko gastiĝas ĉe https://bitbucket.org/bspeakmon/gonehome-eo. Se vi havas
proponojn aŭ trovas cimojn, bonvolu viziti! Mi ĝojos akcepti viajn kontribuaĵojn.

Pli pri Esperanto:
http://lernu.net

Multajn dankojn al The Fullbright Company por mirinda ludospertilo kaj por eldonante
la ilojn, per kiuj mi verkis tiun tradukon!
